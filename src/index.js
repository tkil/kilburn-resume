import "./styles/__import.css";


const main = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const noContact = urlParams.get('nc');
  if(noContact === "true") {
    const el = document.getElementById("contact")
    el.remove();
  }
}

window.addEventListener('DOMContentLoaded', (event) => {
  main();
});
